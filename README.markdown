# Ovchip

Application to read the "OV Chipkaart" (the Dutch public transport card) using the NFC reader on an Android 4.0 ICS phone (tested on the Galaxy Nexus). This App however requires keys which can be extracted using a PC and a compatible card reader.

The application consists of two components: 

- **chipkaart**
  A generic java library to parse and interpret the information stored on the OV Chipkaart and a console app used for hacking and testing.
- **ovcsaldo**
  An Android application designed to run on a Galaxy Nexus running ICS.

Both components are still a work in progress so things can en will go wrong ;-) Fortunately it doesn't try to save anything to the card so that data is safe.

The code is released under the 3-Clause BSD [license].

# Installation

Using the binary is the easiest way, if you prefer to go build from source and familiar with the Android SDK and development tools you can also build from source. 

## Binary

Install get ovcsaldo-date.apk from the [downloads] section.

Create a directory structure on your external storage which is `/sdcard` on the Galaxy Nexus (and not really external). 

    nl.myzt.ovsaldo/
    nl.myzt.ovsaldo/keys/
    nl.myzt.ovsaldo/stations/
    nl.myzt.ovsaldo/dumps/

Copy the stations.sqlite from the [downloads] section to `nl.myzt.ovsaldo/stations/` directory.

Now you can upload one or more card dumps to the phone and open them to import their keys. 

Card dumps can be created using [MOFC] on a PC with a compatible card reader (ACS ACR122U or a Touchatag).

# Usage

- Open the ovcsaldo application. 
- Hold the chipcard to the back of the phone. 

or

- Open the ovcsaldo application.
- Open the card dump you want to analyze.

[downloads]: https://bitbucket.org/myzt/ovchip/downloads
[MOFC]: https://code.google.com/p/nfc-tools/
[license]: http://www.opensource.org/licenses/BSD-3-Clause
