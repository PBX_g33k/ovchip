package nl.myzt.java.android.ovcsaldo;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import nl.myzt.java.android.ovcsaldo.R;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import nl.myzt.ovchipcard.OVCParser;


public class InfoFragment extends Fragment{
		
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
	OVChipData act = (OVChipData) this.getActivity();
	OVCParser ovc_parser = act.getOVCParser(); 
	View vw=inflater.inflate(R.layout.info_layout, container, false);
	TextView tv_data = (TextView) vw.findViewById(R.id.card_data);
	TextView tv_saldo = (TextView) vw.findViewById(R.id.saldo_data);
	Button btn_open = (Button) vw.findViewById(R.id.btn_savedump);
    
    btn_open.setOnClickListener(new android.view.View.OnClickListener() {
      public void onClick(View arg0) {
    	  DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HHmm-ss");
    	  Calendar cal = Calendar.getInstance();
    	  String filename=dateFormat.format(cal.getTime()) + ".dump";
    	    Intent intent = new Intent("org.openintents.action.PICK_FILE");
    	    intent.setData(Uri.parse("file:///sdcard/nl.myzt.ovsaldo/dumps/"+filename));
    	    InfoFragment.this.getActivity().startActivityForResult(intent, 1);
    	    
      }
    });

	tv_data.setText( "" +
			"ID: " + ovc_parser.getID() +
			"\nType: " + ovc_parser.getType() +
			"\nExpires: " + ovc_parser.getExp() +
			"\nBirthday: " + ovc_parser.getBDay());
	String saldo = "" + ovc_parser.getSaldo();
	if (saldo != "" && saldo.charAt(5) == '-') {
		// negativer
		tv_saldo.setTextColor(0xffFF0000);
	}else{
		tv_saldo.setTextColor(0xff00ff00);
	}
	
	tv_saldo.setText(saldo);
	return vw;
	}
	

}
