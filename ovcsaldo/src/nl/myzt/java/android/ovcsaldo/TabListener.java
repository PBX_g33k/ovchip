package nl.myzt.java.android.ovcsaldo;

import nl.myzt.java.android.ovcsaldo.R;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.app.Fragment;


public class TabListener implements android.app.ActionBar.TabListener {

	public Fragment fragment; 
	
	public TabListener(Fragment fragment) {
		this.fragment=fragment;
	}
	
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		//ft.add(R.id. fragment, null);
		// TODO Auto-generated method stub
	}

	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		ft.add(R.id.container, fragment, null);		
	}

	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		ft.remove(fragment);	
	}

}
