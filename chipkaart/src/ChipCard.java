import java.io.*;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;
import java.lang.String;
import nl.myzt.ovchipcard.OVCParser;
import nl.myzt.ovchipcard.OVCTrip;


class ChipCard {
	
	public static void main(String [] args){
		//byte [] data = readData("data/anon.bin");
		byte [] data = readData("data/ns.bin");
		//byte [] data = readData("suus.dump");
		
		OVCParser parser = new OVCParser(data);
		if (data == null) return;
	
		System.out.println("=============================================");
		System.out.println("OV CHIP DUMP READER");
		System.out.println("r0xx0r ze cardz0r");
		System.out.println("");
		System.out.println("=============================================");
		System.out.println("0 GENERAL DATA ");
		System.out.println("=============================================");
		System.out.println("CardID: " + parser.getID());
	    DateFormat df2 = DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.SHORT);
		System.out.println("CardID: " + parser.getID());
		System.out.println("Card type: " + parser.getType());
		System.out.println("Valid until: " + df2.format(parser.getExp()));
		System.out.println("Birthday: " + parser.getBDay());
		
		
		System.out.println("=============================================");
		System.out.println("32-34 SUBSCRIPTIONS");
		System.out.println("=============================================");
		printList(new ArrayList<Object>(parser.getSubscriptions()));

		
		System.out.println("=============================================");
		System.out.println("35-38 TRANSACTIONS");
		System.out.println("=============================================");
		System.out.println("Transactions:");
		parser.parse();
		printList(new ArrayList<Object>(parser.getTransactionHistory()));
		System.out.println("History:");	
		printList(new ArrayList<Object>(parser.getTravelHistory()));
		System.out.println("Charges:");
		printList(new ArrayList<Object>(parser.getChargeHistory()));
		
		
		System.out.println("=============================================");
		System.out.println("39    INDEXES      ");
		System.out.println("=============================================");
		printList(new ArrayList<Object>(OVCTrip.getTripHistory(data)));
		List<String []> keys = parser.getKeys();

		for (int i=0; i<keys.size(); i++){
			System.out.println(""+ keys.get(i)[0] +"\t" + keys.get(i)[1] + "\t"+keys.get(i)[2]);
		}

	}
	
	private static void printList(List<Object> list){
		for (int i=0; i<list.size(); i++){
			System.out.println("[" + Integer.toHexString(i) + "]: " + list.get(i));
		}
	}

	private static byte[] readData(String filename) {
		try {
			File file = new File(filename);
			FileInputStream dumpis = new FileInputStream(file);
			int size=(int)file.length();
			byte[] data = new byte[size];
			dumpis.read(data, 0, size);
			dumpis.close();
			return data;
		} catch (FileNotFoundException fe) {
			System.out.println(fe);
		} catch (IOException fe) {
			System.out.println(fe);
		}
		return null;
	}

	
}