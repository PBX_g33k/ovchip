package nl.myzt.ovchipcard;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;

public class OVCTransaction{
	
	private byte[] rawdata= new byte [32];
	
	private Map<Integer, Integer> trans_data = new HashMap<Integer, Integer>();

	public static final int T_IDENTIFIER=101;
	public static final int T_DAYS=102;
	public static final int T_MINUTES=103;
	public static final int T_UNKNOWN1=1; 
	public static final int T_ACTION=2; 
	public static final int T_COMPANY=4;
	public static final int T_TRANSACTION=6; 
	public static final int	T_STATION=8; 
	public static final int T_MACHINE=10;
	public static final int T_VEHICLE=14;
	public static final int T_UNKNOWN2=16;
	public static final int T_UNKNOWN3=20;
	public static final int T_AMOUNT=23;
	public static final int T_SUBSCRIPTION=25;

	public static int length() {
		return 28;
	}

	public static OVCTransaction getTransaction(byte [] carddata,  int id){
			int cur = 0xb00 + (id*0x20) + ((id/7) * 0x20);
			byte [] buf = new byte[32];
			System.arraycopy(carddata, cur , buf, 0, 32);
			return new OVCTransaction(buf);
	}
	
	public OVCTransaction(byte [] data){
		System.arraycopy(data, 0, rawdata, 0, 32);
		this.parseTransaction();
	}
	
	public String toString(){
	    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
	    NumberFormat nf = NumberFormat.getCurrencyInstance();
	    nf.setCurrency(Currency.getInstance("EUR"));

	  
		return "" + df.format(OVCUtil.toDate(this.trans_data.get(T_DAYS), this.trans_data.get(T_MINUTES))) +   " " 
				+ OVCUtil.ACTIONS.get(this.trans_data.get(T_ACTION)) + " " 
				+ OVCUtil.COMPANY.get(this.trans_data.get(T_COMPANY)) + 
				" \n" + nf.format((double)this.trans_data.get(T_AMOUNT)/100);
	}
	
	public int getAction(){
		return this.getField(T_ACTION);
	}
	
	public int getField(int field){
		if(this.trans_data.containsKey(field)){
			return this.trans_data.get(field);
		}
		return -1;
	}
	
	public boolean isEmpty(){
		if (this.trans_data.get(T_IDENTIFIER) == 0 ) {
			return true;
		}
		return false;
	}
	
    public void parseTransaction(){
    	int counter = 0; 

		trans_data.put(T_IDENTIFIER, (int) OVCUtil.getBits(counter,28,rawdata)); counter += 28;
		trans_data.put(T_DAYS, 	   (int) OVCUtil.getBits(counter,14, rawdata)); counter += 14; 
		trans_data.put(T_MINUTES,    (int) OVCUtil.getBits(counter, 11, rawdata)); counter += 11; 
		
		long wconst = (long) trans_data.get(T_IDENTIFIER);
				
		if (OVCUtil.hasBit(T_UNKNOWN1, wconst)){
			//has unknown constant: 010100101000000000000000
	        counter += 24; 
		}
		if (OVCUtil.hasBit(T_ACTION, wconst)){ 
			//has Action
	        trans_data.put(T_ACTION, (int) OVCUtil.getBits(counter,7, rawdata));
	        counter += 7;
		}
		if (OVCUtil.hasBit(T_COMPANY, wconst)){ 
			//has Company
	        trans_data.put(T_COMPANY, (int) OVCUtil.getBits(counter,16, rawdata));
			counter += 16;
		}
		if (OVCUtil.hasBit(T_TRANSACTION, wconst)) {      
			//has Transaction
	        trans_data.put(T_TRANSACTION, (int) OVCUtil.getBits(counter,24, rawdata));
		    counter += 24;
		}
		if (OVCUtil.hasBit(T_STATION, wconst)) {      
			//has Station
	        trans_data.put(T_STATION, (int) OVCUtil.getBits(counter,16, rawdata));
		    counter += 16;
		}
		if (OVCUtil.hasBit(T_MACHINE, wconst)) {     
			//has Machine
	        trans_data.put(T_MACHINE, (int) OVCUtil.getBits(counter,16, rawdata));
		    counter += 24;	
		}
		if (OVCUtil.hasBit(T_VEHICLE, wconst)) {     
			//has Vehicle
	        trans_data.put(T_VEHICLE, (int) OVCUtil.getBits(counter,16, rawdata));
  	        counter += 16;
		}
		if (OVCUtil.hasBit(T_UNKNOWN2, wconst)) {     
			//has Unknown
			counter += 5;
		}
		if (OVCUtil.hasBit(T_UNKNOWN3, wconst)) {     
			//has Unknown
			counter += 16;
		}
		if (OVCUtil.hasBit(T_AMOUNT, wconst)) {     
			//has Amount
	        trans_data.put(T_AMOUNT, (int) OVCUtil.getBits(counter,16, rawdata));
			counter += 16;
		}
		if (OVCUtil.hasBit(T_SUBSCRIPTION, wconst)) {     
			//has Subscription ID
	        trans_data.put(T_SUBSCRIPTION, (int) OVCUtil.getBits(counter,13, rawdata));
			counter += 13;	
		}
    }
}

