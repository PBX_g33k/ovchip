package nl.myzt.ovchipcard;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.lang.String;

public class OVCParser {
	
	private byte [] carddata = null;
	private List <OVCSubscription>subscriptions  = new ArrayList<OVCSubscription>();
	private List <OVCTransaction>transactions  = null;
	private List <OVCTransaction>history  = null;
	private List <OVCTransaction>charges  = null;



	public OVCParser(byte  [] carddata){ 
		this.carddata = carddata; 	



	}
	public void parse(){
		this.parseSubscriptions();
		this.transactions = OVCTransactionIndex.getTransactionHistory(carddata);
		this.history = OVCTrip.getTravelHistory(carddata);
		this.charges = OVCTransactionIndex.getChargeHistory(carddata);
	}
	public List<OVCTransaction> getTransactionHistory() {
		return this.transactions;
	}
	public List<OVCTransaction> getChargeHistory() {
		return this.charges;
	}
	public List<OVCTransaction> getTravelHistory() {
		return this.history;
	}
	
	public List <OVCSubscription> getSubscriptions() {
		ArrayList<OVCSubscription>subs = new ArrayList<OVCSubscription>();
		for (int i=0; i< this.subscriptions.size(); i++){
			if (!this.subscriptions.get(i).isEmpty()){
				subs.add(this.subscriptions.get(i));
			}
		}
		return subs;
	}
	
	public List<String []> getKeys(){
		List<String []> l = new  ArrayList<String[]>();
		for (int i = 0; i < 40; i++) {
			int ft = this.sectOffset(i + 1) - 0x10;

			byte[] bufA = new byte[6];
			System.arraycopy(this.carddata, ft, bufA, 0, 6);

			byte[] bufB = new byte[6];
			System.arraycopy(this.carddata, ft + 10, bufB, 0, 6);

			String[] keys = new String[3];
			keys[0] = ""+ i;
			keys[1] = OVCUtil.toHex(bufA);
			keys[2] = OVCUtil.toHex(bufB);
			l.add(keys);
		}

		return l;
	}
	
	public void subIdx() {
		byte [] buf = new byte[32];
		System.arraycopy(this.carddata, 0xf10, buf, 0, 32);
		int amount = (int) OVCUtil.getBits(0, 4, buf);
		
		for (int i=0; i<amount; i++) {
			long status = OVCUtil.getBits(4 + i*21, 17, buf);
			int subscription = (int) OVCUtil.getBits(4 + (i*21) + 17, 4, buf) - 1;
			System.out.println("si["+Integer.toHexString(i)+"]: " + subscription + " status: " + OVCUtil.toBin(status, 17));
		}		
	}

	private void parseSubscriptions() {
		for (int sect=32; sect< 35; sect++) {
			int offset = sectOffset(sect);
			for (int i=0; i<5; i++) {
				byte [] buf = new byte[16];
				System.arraycopy(this.carddata, offset + (i * 0x30 ), buf, 0, 16);
				OVCSubscription sub = new OVCSubscription(buf);
				this.subscriptions.add(sub);
			}
		}
	}
	
	public String getID(){
		byte [] buf = new byte[4];
		System.arraycopy(this.carddata, 0, buf, 0, 4);
		return OVCUtil.toHex(buf);
	}
	
	public String getBDay(){
		byte [] year = new byte[2];
		byte [] day = new byte[1];
		byte [] month = new byte[1];
		
		System.arraycopy(this.carddata, 0x58e, year, 0, 2);
		System.arraycopy(this.carddata, 0x58e+2, month, 0, 1);
		System.arraycopy(this.carddata, 0x58e+3, day, 0, 1);
		
		if (year[0] == (byte) 00) {
			return "unknown";
		}

		return OVCUtil.toHex(day) + "-" + OVCUtil.toHex(month) + "-" + OVCUtil.toHex(year);
	}

	public Date getExp(){
		byte [] buf = new byte[3];
		System.arraycopy(this.carddata, 0x1B, buf, 0, 3); //get the bytes where the date is at
		long days = OVCUtil.getBits(0, 20, buf);
		return OVCUtil.toDate(days);
	} 

	public String getType(){
		byte [] buf = new byte[1];
		System.arraycopy(this.carddata, 0x22, buf, 0, 1);

		//we only care about the 2nd word
		int type = Integer.parseInt(String.valueOf(OVCUtil.toHex(buf).charAt(1)),16);

		if (type == 2){
			return "Personal";
		}else if (type == 0){
			return "Anonymous";
		}

		return "unknown: " + OVCUtil.toHex(buf);

	}
	
	public String getSaldo(){
		byte [] buf = new byte[3];

		try{
			System.arraycopy(this.carddata, 0xf99, buf, 0, 3);
		}catch (NullPointerException e){
			return "";
		}
		long sign = OVCUtil.getBits(5, 1, buf);
		
		long amount = OVCUtil.getBits(6, 15, buf);
		if (amount == -0.0){
			return "";
		}
		if (sign == 1){
			return "EUR " + (float) amount/100;
		}else {
			return "EUR -" + (float) amount/100;
		}

	}
		
	
	private int sectOffset(int sector) {
		int offset = 0;
		if (sector > 32) { 
			int nbigsectors=sector-32;
			int nsmallsectors=sector-nbigsectors;
		    offset = nbigsectors * 16 * 16 + ((nsmallsectors) * 4 * 16);
		}else{
			offset = sector * 4 * 16;
		}
		return offset;
	}
}


