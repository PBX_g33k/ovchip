package nl.myzt.ovchipcard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class OVCTrip {
	private int start=0;
	private int size=0;
	private byte [] buf=new byte[32];
	private byte [] carddata; 
	private Map<Integer, Integer> trip_data = new HashMap<Integer, Integer>();
	private OVCTransaction transaction = null;
	
	public static final int ID=101;
	public static final int COMPANY=102;
	public static final int THIDX=103;
	public static final int STATUS=4;
	public static final int UNKNOWN=5;
	
	public static List<OVCTransaction> getTravelHistory(byte [] carddata) {
		List<OVCTransaction> transactions = new ArrayList<OVCTransaction>();
		List<OVCTrip> trips = getTripHistory(carddata);
		for (int i=0; i<trips.size(); i++){
			transactions.add(trips.get(i).getTransaction());
		}
		return transactions;
	}
	
	public static List<OVCTrip> getTripHistory(byte [] carddata) {
		byte [] buf = new byte[32];
		System.arraycopy(carddata, 0xf50, buf, 0, 32);	
		int amount = (int) OVCUtil.getBits(0, 4, buf);
		List<OVCTrip> indexes = new ArrayList<OVCTrip>();
		int cursor=4;
		for (int i=0; i<amount; i++) {
			OVCTrip trip = new OVCTrip(cursor,buf,carddata);
			indexes.add(trip);
			cursor=cursor+trip.getSize();
		}
		return indexes;
	}
	
	public OVCTrip(int start, byte [] buf, byte [] carddata){
		this.start=start;
		this.buf=buf;
		this.carddata=carddata;
		this.size=parseTrip()-start;
	}
	
	public int parseTrip(){
		int cursor=this.start;
        trip_data.put(ID, (int) OVCUtil.getBits(cursor, 6, buf)); cursor += 6;
        int id = trip_data.get(ID);
        trip_data.put(COMPANY, (int) OVCUtil.getBits(cursor, 8, buf)); cursor += 8;
		
		if (OVCUtil.hasBit(STATUS, id)) {     
			// bit2 set -> has status 00 check-out 01 checkin
	        trip_data.put(STATUS, (int) OVCUtil.getBits(cursor, 2, buf)); cursor += 2;
		}
		
        trip_data.put(THIDX,((int) OVCUtil.getBits(cursor, 4, buf)) -1); cursor += 4;
		List<OVCTransaction> th_idx = OVCTransactionIndex.getTravelHistory(this.carddata);	
		this.transaction = th_idx.get(trip_data.get(THIDX));
		
		if (OVCUtil.hasBit(UNKNOWN, id)) {     
			// bit1 set -> has unknown	
	        trip_data.put(UNKNOWN, (int) OVCUtil.getBits(cursor, 4, buf)-1); cursor += 4;
		}
		return cursor;
	}
	
	public OVCTransaction getTransaction(){
		return this.transaction;
	}
	
	public int getSize(){
		return this.size;		
	}
	
	public String toString(){
		String retval = "" + OVCUtil.toBin((long) this.trip_data.get(ID), 6) + " " + transaction;
		if(this.trip_data.containsKey(STATUS)){
			int status = this.trip_data.get(STATUS);
			if (status == 0 ){
				retval += "" + " status: checkedout";

			} else if (status == 1){
				retval += "" + " status: checkedin";

			} else {
				retval += "" + " status: wtf(" + this.trip_data.get(STATUS)+")";
			}
		}
		if(this.trip_data.containsKey(UNKNOWN)){
			retval += "" + " unknown: " + this.trip_data.get(UNKNOWN);

		}
		return  retval;
	}
}
