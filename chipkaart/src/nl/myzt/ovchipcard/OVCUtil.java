package nl.myzt.ovchipcard;

import java.math.BigInteger;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;


public abstract class OVCUtil{
	public static final Map<Integer, String> ACTIONS = 
		    Collections.unmodifiableMap(new HashMap<Integer, String>() {
				private static final long serialVersionUID = 1891636900706334815L;

			{ 
		        put(0, "Purchase");
		        put(1, "Checkin");
		        put(2, "Checkout");
		        put(3, "Transfer");
		        put(6, "Charge");
		    }});
	
	public static final Map<Integer, String> COMPANY = 
		    Collections.unmodifiableMap(new HashMap<Integer, String>() {
				private static final long serialVersionUID = 154399218052686901L;

			{ 
		        put(0, "TLS");
		        put(1, "Connexxion");
		        put(2, "GVB");
		        put(3, "HTM");
		        put(4, "NS");
		        put(5, "RET");
		        put(7, "Veolia");
		        put(8, "Arriva");
		        put(9, "Syntus");
		        put(12, "DUO");
		        put(25, "Appie/Primera");
		    }});
	
	public static final Map<Integer, String> SUBSCRIPTION = 
		    Collections.unmodifiableMap(new HashMap<Integer, String>() {
				private static final long serialVersionUID = 2325681145870738912L;

			{ 
		        put(0x005, "OV-jaarkaart ");
		        put(0x007, "OV-Bijkaart");
		        put(0x019, "Voordeeluren kaart 2-jr");
		        put(0x0ce, "Voordeeluren kaart");
		        put(0x0c9, "Reizen op saldo 1e klas");
		        put(0x0ca, "Reizen op saldo 2e klas");
		        put(0x0e5, "Reizen op saldo temp");
		    }});
	
	public static Date toDate(long days) {
		GregorianCalendar expdate = new GregorianCalendar();
		expdate.set(1997, 0, 1); //1997-Jan-01
		expdate.add(GregorianCalendar.DAY_OF_YEAR, (int) days );
		return expdate.getTime();//return expdate.get(expdate.DAY_OF_MONTH) + "-" +  (1+expdate.get(expdate.MONTH)) + "-" + expdate.get(expdate.YEAR);	
	}
	
	public static Date toDate(long days, int minutes) {
		GregorianCalendar expdate = new GregorianCalendar();
		expdate.set(1997, 0, 1,minutes/60, minutes%60); //1997-Jan-01
		expdate.add(GregorianCalendar.DAY_OF_YEAR, (int) days );
		//return hours + ":" + min;	
		return expdate.getTime();
	}
	
	public static long getBits(int offset, int numBits, byte [] b) {
		BigInteger chunk = new BigInteger(1,b);
		//long bits = Long.parseLong(chunk.toString(2));
		BigInteger bits = new BigInteger(chunk.toString(2));
		String bitmap = String.format("%0" + b.length * 8 + "d\n", bits);
		long data = Long.parseLong(bitmap.substring(offset,numBits+offset),2);
		return data;
	}
	
	public static boolean hasBit (int bit, long chunk) {
		int decnum = (int) Math.pow(2,(double) bit);
		
		if ((chunk & decnum) == decnum){
			return true; 
		}
		return false;
	}
	 
	public static  String toHex(byte [] b) {
		String out = "";
		for (int a=0; a<b.length; a++){
			int i = b[a] & 0xFF;
			if (i < 16){
				out += "0"+Integer.toHexString(i);
			}else{
				out += Integer.toHexString(i);
			}
		}
		return out;
	}
	
	public static String toBin(long value, int size) {
		String binstring = String.format("%"+size+"s",Long.toBinaryString(value)).replace(' ','0');
		String retval = "";
		for (int i=0; i < binstring.length()/4; i++){
			retval += " " + binstring.substring(i*4, (i+1)*4);
		}
		retval += " " + binstring.substring(binstring.length() - binstring.length()%4, binstring.length());
		
		return retval.trim();
	}
	
	public static  String toHex(byte b) {
		String out = "";
		int i = b & 0xFF;
		if (i < 16){
			out += "0"+Integer.toHexString(i);
		}else{
			out += Integer.toHexString(i);
		}

		return out;
	}
	
}